const   NeuralNetwork =    require("./NeuralNetwork.js");
const Agent = require("./Agent.js");
module.exports= class GeneticManager {

    constructor( inputs,hidden,outputs,nAgents, agentsMoney){
        this.agentsMoney = agentsMoney;
        this.nAgents = nAgents;
        this.generation = 0;
        this.bestAgent = {}
        this.agents = [];
        this.inputs = inputs;
        this.hidden = hidden;
        this.outputs = outputs;
        this.initNeuralNetworks(inputs,hidden,outputs)
    }
    // generate n nerual networks with random weights
    initNeuralNetworks( inputs,hidden,outputs){
        for (let i = 0; i < this.nAgents; i++) {
            var agent = new Agent(this.agentsMoney)
            agent.fitnes = 0;
            agent.NN = new NeuralNetwork(inputs,hidden,outputs);
            this.agents.push(agent);       
        }
    }

    execute(input){
       
        var chosen = [0,0,0];
        for (let i = 0; i < this.agents.length; i++) {
            const agent = this.agents[i];
            var action = agent.NN.getPrediction(input);
            chosen[action] += 1;
            if(action === 0) agent.buy(input);
            if(action === 2) agent.sell(input);        
        }
        //console.log(chosen);
        
        return(chosen);
                

    }
    generateNextGen(){
       var totalFit = this.calculateProbabilities().totalFit;
        var newGeneration = []
        for (let i = 0; i < this.nAgents; i++) {
             var a  = this.pickFromPool(totalFit)
             var b = this.pickFromPool(totalFit)
             var agent = new Agent(this.agentsMoney)
             agent.fitnes = 0;
             agent.NN = this.mixAgents(a,b);
             newGeneration.push(agent);    
        }
        this.agents = newGeneration;

    }
    getStats(lastPrice){
        var best = this.agents[0];
        var worst = this.agents[0];
        var average = 0;
        this.agents.forEach(agent => {
            agent.sell(lastPrice);
            agent.fitnes = agent.getFinalPoints();
            if(agent.fitnes>best.fitnes) best = agent;
            if(agent.fitnes<worst.fitnes) worst = agent;
            average+= agent.fitnes;
        });
        return{
            "best":best,
            "worst":worst,
            "average":average/this.agents.length,
        }
    }
   
    getAgent(index){
        return this.agents[index].NN;
    }
    setFitnesOf(index,fitnes){
        this.agents[index].fitnes = fitnes;
    }
     // with the fitnes of each one we put them in a pool 
    calculateProbabilities(){
        var maxFit = this.agents[0].fitnes;
        var totalFitnes = 0;
        for (let i = 0; i < this.agents.length; i++) {
            const fit = this.agents[i].fitnes;
            if(fit>maxFit) maxFit = fit;
            totalFitnes+= fit;
        }
        return {"totalFit":totalFitnes,"maxFit":maxFit}

    }
    pickFromPool(totalFitnes){
        // full random
        if(this.randomBetwen(0,1)<this.randomBias){
            var agent = new Agent(this.agentsMoney)
            agent.fitnes = 0;
            agent.NN = new NeuralNetwork(this.inputs,this.hidden,this.outputs);
            //console.log("full random");
            
            return agent;
        }
        let limit = this.randomBetwen(0,totalFitnes);
        let i = 0;
        let somme = 0;
         do  {
            somme += this.agents[i].fitnes;
            i++;
        } while (somme < limit )
        if(this.agents[i] != null){
            return this.agents[i];
        }else{
            //console.log("pick falló y uso random",somme,limit,totalFitnes);
            return this.agents[this.randomBetwen(0,this.agents.length-1)];
        }
        
    }
    // mix the weights but REMEMBER TO ADD SOM RANDOM PARTS 
    mixAgents(a,b){
        var aW = a.NN.getWeights();
        var bW = b.NN.getWeights();

        var newWeights = []
        //de cada capa
        for (let i = 0; i < aW.length; i++) {
            const layer = aW[i];
            newWeights.push([])
            // de cada fila
            for (let j = 0; j < layer.length; j++) {
                const line = layer[j];
                newWeights[i].push([])
                // de cada columna
                for (let k = 0; k < line.length; k++) {
                     newWeights[i][j][k] = (aW[i][j][k] + bW[i][j][k])/2
                }
            }
            
        }

            a.NN.setWeights(newWeights)
            return a.NN;
    }

    randomBetwen(a,b){
        return  Math.floor(a +(Math.random()*(b-a)));
    }

}