module.exports =
class Perceptron{

    constructor(nInputs){
        var inputs =[]
        this.weights =[]
        this.lr = 0.05;
        for (let i = 0; i < nInputs; i++) {
            this.weights.push(Math.random());
            
        }
    }
    sign(n){
        return(n>0)?1:-1;
    }
    fix(inputs, desOutput){
         var mal = 0;
         const output = this.execute(inputs);
         const error = desOutput - output;
        
         for (let i = 0; i < this.weights.length; i++) {
            this.weights[i]+=error * inputs[i] * this.lr;   
         }
         return error;
         
    }

    execute(inputs){        
        var result = 0;
        for (let i = 0; i < inputs.length; i++) {
           result += inputs[i] * this.weights[i];    
        }
        return this.sign(result);
    }
}
