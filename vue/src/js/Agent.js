module.exports = class Agent{
    constructor(iniMoney){
       this.iniMoney = iniMoney;
       this.money = iniMoney;
       this.bought = 0;
       this.selled = 0;
       this.currentStock = -1;
    }

    buy(price){
        price = Number(price);
        if(!this.haveStock() && this.money>= price){
        this.bought +=1;
        this.currentStock = price;
        this.money -= price;
        }

    }
    haveStock(){
        return (this.currentStock!=-1)
    }

    sell(price){
        price = Number(price)
        if(this.haveStock()){
        this.money += price;
        this.currentStock = -1;
        this.selled +=1;
        
        }
        
    }

    getFinalPoints(){
        var total = this.money ;
        this.currentStock = Number(this.currentStock);
        total+= this.haveStock() ?  this.currentStock : 0 ;
            return 1+Math.pow( total- (this.iniMoney/2),2); //+ ((total/1+this.bought)/10)
    }

}