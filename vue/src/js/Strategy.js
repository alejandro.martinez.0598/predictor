const Agent = require("./Agent.js");
module.exports = class Strategy{
    constructor(iniMoney){
       this.steps = 1;
       this.incrementToSell = 0.1;
       this.decrementToBuy = 0.1;
       this.longMemoryDays =  0;
       this.shortMemoryDays =  0;
       this.agent = new Agent(0);

    }

    buy(price){
        price = Number(price);
        if(!this.haveStock() && this.money>= price){
        this.bought +=1;
        this.currentStock = price;
        this.money -= price;
        }

    }
    haveStock(){
        return (this.currentStock!=-1)
    }

    sell(price){
        price = Number(price)
        if(this.haveStock()){
        this.money += price;
        this.currentStock = -1;
        this.selled +=1;
        
        }
        
    }

    getFinalPoints(){
        var total = this.money ;
        this.currentStock = Number(this.currentStock);
        total+= this.haveStock() ?  this.currentStock : 0 ;
            return 1+Math.pow( total- (this.iniMoney/2),2); //+ ((total/1+this.bought)/10)
    }

}