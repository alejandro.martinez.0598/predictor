module.exports = class NeuralNetwork{
    constructor(nInputs,nMidleLayer,nOutputs){
        this.lr = 0.05;
        this.nInputs = nInputs;
        this.nMidleLayer = nMidleLayer;
        this.wheights = this.generateRangdomWeights(nInputs,nMidleLayer)
        this.nOutputs = nOutputs;
        this.outputWeights = this.generateRangdomWeights(nMidleLayer,nOutputs);
    }

    execute(input){
    var result = this.feedForward(
                this.feedForward(input,this.wheights),
                this.outputWeights
    )
    
    //console.log(result);
    return result;
    
    }
    getPrediction (inputs){
        return this.getBigestIndex(this.execute(inputs))
    }
    getBigestIndex(inputs){
        var max = 0
        for (let i = 0; i < inputs.length; i++) {
            const element = inputs[i];
            if(element>inputs[max]){
                max = i;
            }
            
        }
        return max;
    }
    fix(input,desiredResult){
        var desiredresults = [];
        // we  feed the NN with the data and get the result
        var result = this.execute(input);
        
        var errors = []
        // get the initial errors of each output neuron;
        for(var i = 0; i< this.nOutputs;i++){
            // if this is the neurone representing the good answer we put a 1
            desiredresults.push(desiredResult ==i? 1:0);
            errors.push(desiredresults[i]-result[i]);
        }
        //console.log("final errors",errors);
        
        this.backPropagation( 
            this.backPropagation(errors,this.outputWeights),
            this.wheights
        )

        // esta condicion decide si el resultado fue correcto o no 
       var  right = (desiredResult == this.getBigestIndex(result));
         return right;
    }
    getWeights(){
        return [this.wheights,this.outputWeights]
    }
    setWeights(nWeights){
            this.wheights = nWeights[0];
            this.outputWeights = nWeights[1];
    }
    train(dataset,results){
        var correctAnswers = 0;
       for (let i = 0; i < dataset.length; i++) {
           const row = dataset[i];
           correctAnswers += this.fix(row,results[i])?1:0;
       }
       const acuracy = correctAnswers/dataset.length;
       //console.log("acuracy",acuracy);
       return acuracy;
        
    }

    feedForward(inputs,wheights){
        if(inputs.length == wheights.length){
            var results=[];
            for (let j = 0; j < wheights[0].length; j++) {
                 results.push(0);
            }

            // multypling inputs withs wheights
            for (let i = 0; i < inputs.length; i++) {
                
                for (let j = 0; j < wheights[i].length; j++) {
                    
                      results[j] += this.reLu(wheights[i][j] * inputs[i]);
                    
                }
             }
             return results;

        }
        else{
            console.log("wrong number of inputs ", inputs,wheights)
        }
    }
    reLu (input){
        return (input>0)? input:input/4;
    }
    backPropagation(errors,wheights){
        if(errors.length == wheights[0].length){
            var nErrors=[];
            for (let j = 0; j < wheights.length; j++) {
                 nErrors.push(0);
            }

            // multypling inputs withs wheights
            for (let i = 0; i < errors.length; i++) {
                
                for (let j = 0; j < wheights[i].length; j++) {
                    var error = wheights[i][j] * errors[i] * this.lr;
                    wheights[i][j] += error;
                    nErrors[i] += error;
                }
             }
            return nErrors;

        }
        else{
            console.log("wrong number of inputs ", inputs,wheights)
        }
    }


    generateRangdomWeights(nInputs,nMidleLayer){
        var matrix = [];
            for(var i =0 ; i < nInputs; i++){
                matrix.push([]);
                for(var k =0 ; k < nMidleLayer; k++){
                    matrix[i].push(-1 +(Math.random()*2));
                }
            }
        return matrix;
    }
}